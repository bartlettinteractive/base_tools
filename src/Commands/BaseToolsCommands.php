<?php

namespace Drupal\base_tools\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class BaseToolsCommands extends DrushCommands {

  /**
   * Create a sub-theme of Base.
   *
   * @param $name
   *   The name of your sub-theme.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @option machine-name
   *   [a-z, 0-9, _] A machine-readable name for your sub-theme.
   * @option description
   *   A description of your sub-theme.
   * @option path
   *   The path where your sub-theme will be created. Defaults to: themes/custom
   *
   * @usage drush bgt "Theme name"
   *   Create a sub-theme, using the default options.
   * @usage drush bgt "Theme name" --path=themes --description="This is a theme."
   *   Create a sub-theme in the specified directory with a custom description.
   *
   * @command base:generate
   * @aliases bgt,base-generate
   */
  public function generate($name, array $options = ['machine-name' => NULL, 'description' => NULL, 'path' => NULL]) {
    $machine_name = $options['machine-name'];
    $description = $options['description'];
    $path = $options['path'];

    // Set machine name if needed.
    if (!$machine_name) {
      $machine_name = !empty($machine_name) ? preg_replace('/[^a-z0-9_-]+/', '', strtolower($machine_name)) : preg_replace('/[^a-z0-9_-]+/', '', strtolower($name));
      $machine_name = str_replace('-', '_', $machine_name);
    }

    // Clean up the machine name.
    $machine_name = str_replace(' ', '_', strtolower($machine_name));
    $search = ['/[^a-z0-9_]/', '/^[^a-z]+/'];
    $machine_name = preg_replace($search, '', $machine_name);

    // Determine the path to the new sub-theme.
    $subtheme_path = 'themes/custom';
    if ($path) {
      $subtheme_path = drush_trim_path($path);
    }

    // Check if custom directory exists.
    if (!is_dir(drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . $subtheme_path)) {
      mkdir(drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . $subtheme_path, 0755, TRUE);
    }

    $subtheme_dir = drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . $subtheme_path . '/' . $machine_name;
    $starter_dir = drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . drupal_get_path('theme', 'base') . '/STARTER';

    // Copy the sub-theme.
    $this->output()->writeln(dt('Generating theme...'));
    if (!drush_copy_dir($starter_dir, $subtheme_dir)) {
      return FALSE;
    }

    // Rename files and fill in the theme machine name
    drush_op('rename', "$subtheme_dir/STARTER.info.yml", "$subtheme_dir/$machine_name.info.yml");
    drush_op('rename', "$subtheme_dir/STARTER.libraries.yml", "$subtheme_dir/$machine_name.libraries.yml");
    drush_op('rename', "$subtheme_dir/STARTER.breakpoints.yml", "$subtheme_dir/$machine_name.breakpoints.yml");
    drush_op('rename', "$subtheme_dir/STARTER.theme", "$subtheme_dir/$machine_name.theme");

    // Update the .info file
    drush_op([$this, 'fileStrReplace'], "$subtheme_dir/$machine_name.info.yml", 'Base Starter', "$name");

    // Change the description of the theme
    if (!empty($description)) {
      drush_op([$this, 'fileStrReplace'], "$subtheme_dir/$machine_name.info.yml", 'Custom subtheme based on the the Base theme.', "$description");
    }

    // Replace STARTER in info.yml
    drush_op([$this, 'fileStrReplace'], "$subtheme_dir/$machine_name.info.yml", 'STARTER', "$machine_name");

    // Rename functions in .theme
    drush_op([$this, 'fileStrReplace'], "$subtheme_dir/$machine_name.theme", 'STARTER', "$machine_name");

    // Replace STARTER in package.json
    drush_op([$this, 'fileStrReplace'], "$subtheme_dir/package.json", 'STARTER', "$machine_name");

    // Replace STARTER in gulpfile.js
    drush_op([$this, 'fileStrReplace'], "$subtheme_dir/gulpfile.js", 'STARTER', "$machine_name");

    // Notify user of the newly created theme.
    $this->output()->writeln(dt("\n!name was created in !path. \n",
      array(
        '!name' => $name,
        '!path' => $subtheme_path,
      )
    ));
  }

  /**
   * Helper function to replace strings in a file.
   */
  public function fileStrReplace($file_path, $find, $replace) {
    $file_contents = file_get_contents($file_path);
    $file_contents = str_replace($find, $replace, $file_contents);
    file_put_contents($file_path, $file_contents);
  }

}
